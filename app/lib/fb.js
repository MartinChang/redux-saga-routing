/*
	fb lib
	by Zap
*/
export function getFBPic(fbid){
	return `https://graph.facebook.com/${fbid}/picture`
}