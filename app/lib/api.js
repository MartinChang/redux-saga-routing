import querystring from 'querystring'
import fetch from 'isomorphic-fetch'
import { getCookie } from './cookie.js'

const DOMAIN = 'gamania'
const API_URI = 'http://163.22.21.48:8011'
const API_CMD = {
	qa: 'QARobot.aspx',
	fb: 'API/FBLogin.aspx',
	qs: 'API/getQueryStrings.aspx',
}

export default function(cmd='qa',param={}){
	const qs = querystring.stringify({
		domain: DOMAIN,
		passportcode: getCookie('PassportCode'),
		account: getCookie('Account'),
		...param
	})
	return fetch(`${API_URI}/${API_CMD[cmd]}?${qs}`)
}