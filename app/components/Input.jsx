import React, { PropTypes } from 'react'

let input

const Input = ({ placeholder,addmsg }) => (
	<form onSubmit={submitHandler(addmsg)}>
		<input placeholder={placeholder} ref={node=>{
			input = node
			if (input != null) input.focus()
		}}/>
	</form>
)


const submitHandler = addmsg=>e=>{
	e.preventDefault();
	if (!input.value.trim()) return
	addmsg(input.value)
	input.value = ''
	input.focus()
}

Input.propTypes = {
	placeholder: PropTypes.string,
	addmsg: PropTypes.func
}

export default Input