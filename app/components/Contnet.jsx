import React, { PropTypes } from 'react'

const Content = ({ str }) => (
	<div>
		<h1>This is subPage</h1>
		<h3>Route: {str}</h3>
	</div>
)

Content.propTypes = {
	str: PropTypes.string
}

export default Content