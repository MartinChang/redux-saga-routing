import React, { PropTypes } from 'react'
import { Link } from 'react-router'

const List = ({msgs}) => {
	return (<ul>
			{msgs.map(msg=>(
				<li><Link to={`/${msg}`}>{msg}</Link></li>
			))}	
	</ul>)
}

List.propTypes = {
	msgs: PropTypes.arrayOf(PropTypes.string)
}

export default List