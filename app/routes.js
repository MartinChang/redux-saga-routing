import React from 'react'
import { Route } from 'react-router'
import App from './container/App.jsx'
import ContentPage from './container/ContentPage.jsx'

export default (
  <Route path='/' component={App}>
    <Route path='/:str' component={ContentPage} />
  </Route>
)