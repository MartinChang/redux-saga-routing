import { PRINT,LOG } from '../actions'

export default function(state={
	prints:[]
},action){
	switch(action.type) {
		case LOG:
			return state
		case PRINT:
			return {
				prints:[...state.prints,action.msg]
			}
		default:
			return state
	}
}