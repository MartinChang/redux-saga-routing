import { combineReducers } from 'redux'
import { syncHistoryWithStore, routerReducer } from 'react-router-redux'

import msg from './msg.js'

const rootReducer = combineReducers({
	msg,
	routing: routerReducer
})

export default rootReducer