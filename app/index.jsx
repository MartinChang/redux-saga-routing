import React from 'react'
import { render } from 'react-dom'
import Root from './container/Root.jsx'
import configureStore from './store'
import { TITLE } from './actions'
import rootSaga from './sagas'
import { browserHistory } from 'react-router'
import { syncHistoryWithStore } from 'react-router-redux'

const store = configureStore() 
const history = syncHistoryWithStore(browserHistory, store)

store.runSaga(rootSaga)

document.title = TITLE

render(
	<Root store={store} history={history} />
	,document.getElementById('section')
)