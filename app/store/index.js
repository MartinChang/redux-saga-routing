import { createStore, applyMiddleware } from 'redux'
import createLogger from 'redux-logger'
import rootReducer from '../reducers'
import createSagaMiddleware from 'redux-saga'

const loggerMiddleware = createLogger()
const sagaMiddleware = createSagaMiddleware()

export default function configureStore(initialState) {
	return {
		...createStore(
			rootReducer,
			initialState,
			applyMiddleware(
				loggerMiddleware,
				sagaMiddleware
			)
		),
		runSaga: sagaMiddleware.run
	}
}

