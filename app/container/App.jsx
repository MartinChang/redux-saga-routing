import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import InputForm from '../container/InputForm.jsx'
import List from '../components/List.jsx'

class App extends Component {
  constructor(props) {
    super(props)
  }

  render() {  
    let {children,msgs} = this.props
  	let {prints} = msgs
    return (
    	<div>
    		<InputForm/>
    		<List msgs={prints}/>
        <hr/>
        {children}
    	</div>
    )
  }
}

App.propTypes = {
  children: PropTypes.node,
  msgs: PropTypes.arrayOf(PropTypes.string),
}



function mapStateToProps(state,ownProps) {
  return {
  	msgs: state.msg
  } 
}


export default connect(mapStateToProps)(App)
