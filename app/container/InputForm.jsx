import { connect } from 'react-redux'
import Input from '../components/Input.jsx'
import { logmsg } from '../actions'

const mapStateToProps = (state) => {
	return {
		placeholder: '請輸入想要 Route 的路徑'
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		addmsg: msg=>dispatch(logmsg(msg))
	}
}

const InputForm = connect(
	mapStateToProps,
	mapDispatchToProps
)(Input)

export default InputForm	