import { fork,take,put } from 'redux-saga/effects'
import { PRINT,LOG,addmsg } from '../actions'


function* print(){
	while (true) {	
		const {msg} = yield take(LOG)
		yield put(addmsg(msg))
  	}
}


export default function* () {
  yield fork(print)
}